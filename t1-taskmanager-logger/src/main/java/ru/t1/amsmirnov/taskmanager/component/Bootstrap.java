package ru.t1.amsmirnov.taskmanager.component;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.amsmirnov.taskmanager.api.ILoggerService;
import ru.t1.amsmirnov.taskmanager.api.IPropertyService;
import ru.t1.amsmirnov.taskmanager.api.IReceiverService;
import ru.t1.amsmirnov.taskmanager.listener.LoggerListener;
import ru.t1.amsmirnov.taskmanager.service.PropertyService;
import ru.t1.amsmirnov.taskmanager.service.ReceiverService;

import javax.jms.ConnectionFactory;

@Component
public class Bootstrap {

    @NotNull
    private static final Logger logger = Logger.getLogger(Bootstrap.class);

    @NotNull
    @Autowired
    private IReceiverService receiverService;

    @NotNull
    @Autowired
    private LoggerListener loggerListener;

    public void init() {
        try {
           receiverService.receive(loggerListener);
        } catch (final Exception e) {
            logger.error(e);
        }
    }

}
