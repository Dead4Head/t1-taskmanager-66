package ru.t1.amsmirnov.taskmanager.api;

import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.dto.ProjectWebDto;
import ru.t1.amsmirnov.taskmanager.enumerated.Status;

import java.util.List;

public interface IProjectRestEndpoint {

    @Nullable
    ProjectWebDto create(String name, String desc);

    @Nullable
    List<ProjectWebDto> findAll();

    @Nullable
    ProjectWebDto findById(String id);

    @Nullable
    ProjectWebDto save(ProjectWebDto project);

    @Nullable
    ProjectWebDto delete(ProjectWebDto project);

    @Nullable
    ProjectWebDto delete(String id);

    void deleteAll();

}
