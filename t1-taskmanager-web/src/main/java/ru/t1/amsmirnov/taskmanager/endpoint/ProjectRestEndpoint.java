package ru.t1.amsmirnov.taskmanager.endpoint;

import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import ru.t1.amsmirnov.taskmanager.api.IProjectRestEndpoint;
import ru.t1.amsmirnov.taskmanager.dto.ProjectWebDto;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.service.dto.ProjectDtoService;

import java.util.List;

@RestController
@RequestMapping(value = "/api/projects", produces = "application/json")
public class ProjectRestEndpoint implements IProjectRestEndpoint {

    @Autowired
    private ProjectDtoService projectService;

    @Nullable
    @Override
    @PostMapping("/create")
    public ProjectWebDto create(
            @RequestParam("name") String name,
            @RequestParam(value = "description", defaultValue = "") String desc
    ) {
        try {
            return projectService.create(name, desc);
        } catch (AbstractException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
        }
    }

    @Override
    @GetMapping("/findAll")
    public List<ProjectWebDto> findAll() {
        try {
            return projectService.findAll();
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage(), e);
        }
    }

    @Override
    @GetMapping("/findById/{id}")
    public ProjectWebDto findById(@PathVariable("id") final String id) {
        try {
            return projectService.findOneById(id);
        } catch (AbstractException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage(), e);
        }
    }

    @Override
    @PostMapping("/save")
    public ProjectWebDto save(@RequestBody final ProjectWebDto project) {
        try {
            return projectService.update(project);
        } catch (AbstractException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
        }
    }

    @Override
    @DeleteMapping("/delete")
    public ProjectWebDto delete(@RequestBody final ProjectWebDto project) {
        try {
            return projectService.removeOne(project);
        } catch (AbstractException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
        }
    }

    @Override
    @DeleteMapping("/delete/{id}")
    public ProjectWebDto delete(@PathVariable("id") final String id) {
        try {
            return projectService.removeOneById(id);
        } catch (AbstractException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
        }
    }

    @Override
    @DeleteMapping("/deleteAll")
    public void deleteAll() {
        try {
            projectService.removeAll();
        } catch (AbstractException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
        }
    }

}
