package ru.t1.amsmirnov.taskmanager.controller.task;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.amsmirnov.taskmanager.dto.TaskWebDto;
import ru.t1.amsmirnov.taskmanager.enumerated.Status;
import ru.t1.amsmirnov.taskmanager.service.dto.ProjectDtoService;
import ru.t1.amsmirnov.taskmanager.service.dto.TaskDtoService;

@Controller
public class TaskController {

    @Autowired
    private TaskDtoService taskService;

    @Autowired
    private ProjectDtoService projectService;

    @PostMapping("/task/create")
    public ModelAndView create() {
        try {
            taskService.add(new TaskWebDto("Task_" + System.currentTimeMillis()));
            return new ModelAndView("redirect:/tasks");
        } catch (Exception e) {
            return errorView(e);
        }
    }

    @PostMapping("/task/delete/{id}")
    public ModelAndView delete(@PathVariable("id") String id) {
        try {
            taskService.removeOneById(id);
            return new ModelAndView("redirect:/tasks");
        } catch (Exception e) {
            return errorView(e);
        }
    }

    @GetMapping("/task/edit/{id}")
    public ModelAndView edit(@PathVariable("id") String id) {
        try {
            final TaskWebDto task = taskService.findOneById(id);
            if (task == null)
                return new ModelAndView("redirect:/tasks");
            ModelAndView modelAndView = new ModelAndView();
            modelAndView.setViewName("task-edit");
            modelAndView.addObject("task", task);
            modelAndView.addObject("statuses", Status.values());
            modelAndView.addObject("projects", projectService.findAll());
            return modelAndView;
        } catch (Exception e) {
            return errorView(e);
        }
    }

    @PostMapping("/task/edit/{id}")
    public ModelAndView edit(@ModelAttribute("task") TaskWebDto task, BindingResult result) {
        try {
            if ("".equals(task.getProjectId())) task.setProjectId(null);
            taskService.update(task);
            return new ModelAndView("redirect:/tasks");
        } catch (Exception e) {
            return errorView(e);
        }
    }

    private ModelAndView errorView(final Exception e) {
        final ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("error");
        modelAndView.addObject("error", e);
        return modelAndView;
    }

}
