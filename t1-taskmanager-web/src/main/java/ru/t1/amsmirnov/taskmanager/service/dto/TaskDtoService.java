package ru.t1.amsmirnov.taskmanager.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.amsmirnov.taskmanager.dto.TaskWebDto;
import ru.t1.amsmirnov.taskmanager.enumerated.Status;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.exception.field.IdEmptyException;
import ru.t1.amsmirnov.taskmanager.exception.field.NameEmptyException;
import ru.t1.amsmirnov.taskmanager.exception.field.ProjectIdEmptyException;
import ru.t1.amsmirnov.taskmanager.repository.dto.TaskDtoRepository;

import java.util.List;

@Service
public class TaskDtoService
        extends AbstractModelDtoService<TaskWebDto, TaskDtoRepository> {

    @Autowired
    public TaskDtoService(@NotNull final TaskDtoRepository repository) {
        super(repository);
    }

    @NotNull
    @Transactional
    public TaskWebDto create(
            @Nullable final String name,
            @Nullable final String description
    ) throws AbstractException {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final TaskWebDto task = new TaskWebDto();
        task.setName(name);
        task.setDescription(description);
        return add(task);
    }

    @NotNull
    public List<TaskWebDto> findAllByProjectId(
            @Nullable final String projectId
    ) throws AbstractException {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        return repository.findAllByProjectId(projectId);
    }

    @NotNull
    @Transactional
    public TaskWebDto updateById(
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull final TaskWebDto task = findOneById(id);
        task.setName(name);
        task.setDescription(description);
        return update(task);
    }

    @NotNull
    @Transactional
    public TaskWebDto changeStatusById(
            @Nullable final String id,
            @Nullable final Status status
    ) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final TaskWebDto task = findOneById(id);
        if (status == null) return task;
        task.setStatus(status);
        return update(task);
    }

}
