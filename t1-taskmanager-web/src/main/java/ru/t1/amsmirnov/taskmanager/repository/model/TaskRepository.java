package ru.t1.amsmirnov.taskmanager.repository.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.model.TaskWeb;

import java.util.List;

public interface TaskRepository extends AbstractRepository<TaskWeb> {

    @NotNull
    List<TaskWeb> findAllByProjectWebId(@NotNull final String projectWebId);

    void deleteByProjectWebId(final @NotNull String projectWebId);

}
