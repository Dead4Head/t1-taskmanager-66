package ru.t1.amsmirnov.taskmanager.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import ru.t1.amsmirnov.taskmanager.exception.entity.ModelNotFoundException;

@ControllerAdvice
public class AbstractExceptionAdvice {

    @ExceptionHandler(AbstractException.class)
    public ResponseEntity<ExceptionResponse> handleException(AbstractException e) {
        ExceptionResponse response = new ExceptionResponse(e);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @ExceptionHandler(ModelNotFoundException.class)
    public ResponseEntity<ExceptionResponse> handleNotFoundException(ModelNotFoundException e) {
        ExceptionResponse response = new ExceptionResponse(HttpStatus.NOT_FOUND, e);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

}
