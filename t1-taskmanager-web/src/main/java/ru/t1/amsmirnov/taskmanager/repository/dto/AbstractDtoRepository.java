package ru.t1.amsmirnov.taskmanager.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;
import ru.t1.amsmirnov.taskmanager.dto.AbstractModelDto;

import java.util.Optional;

@NoRepositoryBean
public interface AbstractDtoRepository<M extends AbstractModelDto> extends JpaRepository<M, String> {

    void deleteById(@NotNull final String id);

    @NotNull
    Page<M> findAll(@NotNull Pageable pageable);

    @NotNull
    Optional<M> findById(@NotNull final String id);

    boolean existsById(@NotNull final String id);

}
