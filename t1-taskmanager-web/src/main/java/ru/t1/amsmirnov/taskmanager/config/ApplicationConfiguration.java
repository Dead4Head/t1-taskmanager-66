package ru.t1.amsmirnov.taskmanager.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;

@Import(DatabaseConfiguration.class)
@ComponentScan("ru.t1.amsmirnov.taskmanager")
public class ApplicationConfiguration {

}
