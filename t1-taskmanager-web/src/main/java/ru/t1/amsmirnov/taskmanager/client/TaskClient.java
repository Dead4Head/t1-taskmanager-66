package ru.t1.amsmirnov.taskmanager.client;

import feign.Feign;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.web.HttpMessageConverters;
import org.springframework.cloud.netflix.feign.support.SpringDecoder;
import org.springframework.cloud.netflix.feign.support.SpringEncoder;
import org.springframework.cloud.netflix.feign.support.SpringMvcContract;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.web.bind.annotation.*;
import ru.t1.amsmirnov.taskmanager.dto.TaskWebDto;

import java.util.List;

public interface TaskClient {

    static TaskClient client(final String baseUrl) {
        final FormHttpMessageConverter converter = new FormHttpMessageConverter();
        final HttpMessageConverters converters = new HttpMessageConverters(converter);
        final ObjectFactory<HttpMessageConverters> objectFactory = () -> converters;
        return Feign.builder()
                .contract(new SpringMvcContract())
                .encoder(new SpringEncoder(objectFactory))
                .decoder(new SpringDecoder(objectFactory))
                .target(TaskClient.class, baseUrl);
    }

    @GetMapping("/findAll")
    List<TaskWebDto> findAll();

    @GetMapping("/findById/{id}")
    TaskWebDto findById(@PathVariable("id") final String id);

    @PostMapping("/save")
    TaskWebDto save(@RequestBody final TaskWebDto project);

    @DeleteMapping("/delete")
    TaskWebDto delete(@RequestBody final TaskWebDto project);

    @DeleteMapping("/delete/{id}")
    TaskWebDto delete(@PathVariable("id") final String id);

    @DeleteMapping("/deleteAll")
    void deleteAll();
}
