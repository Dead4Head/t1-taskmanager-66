package ru.t1.amsmirnov.taskmanager.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.amsmirnov.taskmanager.dto.TaskWebDto;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.exception.entity.ModelNotFoundException;
import ru.t1.amsmirnov.taskmanager.exception.entity.ProjectNotFoundException;
import ru.t1.amsmirnov.taskmanager.exception.entity.TaskNotFoundException;
import ru.t1.amsmirnov.taskmanager.exception.field.ProjectIdEmptyException;
import ru.t1.amsmirnov.taskmanager.exception.field.TaskIdEmptyException;
import ru.t1.amsmirnov.taskmanager.repository.dto.TaskDtoRepository;

import java.util.List;
import java.util.Optional;

@Service
public class ProjectTaskDtoService {

    @NotNull
    @Autowired
    private TaskDtoRepository taskRepository;

    @NotNull
    @Autowired
    private ProjectDtoService projectService;

    public ProjectTaskDtoService() {
    }

    @Transactional
    public void bindTaskToProject(
            @Nullable final String projectId,
            @Nullable final String taskId
    ) throws AbstractException {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        if (!projectService.existById(projectId)) throw new ProjectNotFoundException();
        final Optional<TaskWebDto> task = taskRepository.findById(taskId);
        if (!task.isPresent()) throw new TaskNotFoundException();
        final TaskWebDto taskWeb = task.get();
        taskWeb.setProjectId(projectId);
        taskRepository.save(taskWeb);
    }

    @Transactional
    public void removeProjectById(
            @Nullable final String projectId
    ) throws AbstractException {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (!projectService.existById(projectId)) throw new ProjectNotFoundException();
        final List<TaskWebDto> tasks = taskRepository.findAllByProjectId(projectId);
        if (tasks == null) throw new ModelNotFoundException("TaskList");
        taskRepository.deleteByProjectId(projectId);
        projectService.removeOneById(projectId);
    }

    @Transactional
    public void unbindTaskFromProject(
            @Nullable final String projectId,
            @Nullable final String taskId
    ) throws AbstractException {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        if (!projectService.existById(projectId)) throw new ProjectNotFoundException();
        final Optional<TaskWebDto> task = taskRepository.findById(taskId);
        if (!task.isPresent()) throw new TaskNotFoundException();
        final TaskWebDto taskWebDto = task.get();
        taskWebDto.setProjectId(null);
        taskRepository.save(taskWebDto);
    }

}
