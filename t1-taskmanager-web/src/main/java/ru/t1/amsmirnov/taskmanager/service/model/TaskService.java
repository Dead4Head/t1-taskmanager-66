package ru.t1.amsmirnov.taskmanager.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.amsmirnov.taskmanager.enumerated.Status;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.exception.field.IdEmptyException;
import ru.t1.amsmirnov.taskmanager.exception.field.NameEmptyException;
import ru.t1.amsmirnov.taskmanager.exception.field.ProjectIdEmptyException;
import ru.t1.amsmirnov.taskmanager.exception.field.UserIdEmptyException;
import ru.t1.amsmirnov.taskmanager.model.TaskWeb;
import ru.t1.amsmirnov.taskmanager.repository.model.TaskRepository;

import java.util.List;

@Service
public class TaskService extends AbstractModelService<TaskWeb, TaskRepository> {

    @Autowired
    public TaskService(@NotNull final TaskRepository repository) {
        super(repository);
    }

    @NotNull
    @Transactional
    public TaskWeb create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final TaskWeb task = new TaskWeb();
        task.setName(name);
        task.setDescription(description);
        return add(task);
    }


    @NotNull
    public List<TaskWeb> findAllByProjectId(
            @Nullable final String userId,
            @Nullable final String projectId
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        return repository.findAllByProjectWebId(projectId);
    }


    @NotNull
    @Transactional
    public TaskWeb updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull final TaskWeb task = findOneById(id);
        task.setName(name);
        task.setDescription(description);
        return update(task);
    }

    @NotNull
    @Transactional
    public TaskWeb changeStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final TaskWeb task = findOneById(id);
        if (status == null) return task;
        task.setStatus(status);
        return update(task);
    }

}
