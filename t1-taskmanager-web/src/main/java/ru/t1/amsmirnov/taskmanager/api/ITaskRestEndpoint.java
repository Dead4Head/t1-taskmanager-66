package ru.t1.amsmirnov.taskmanager.api;

import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.dto.TaskWebDto;

import java.util.List;

public interface ITaskRestEndpoint {

    @Nullable
    List<TaskWebDto> findAll();

    @Nullable
    TaskWebDto findById(String id);

    @Nullable
    TaskWebDto save(TaskWebDto project);

    @Nullable
    TaskWebDto delete(TaskWebDto project);

    @Nullable
    TaskWebDto delete(String id);

    void deleteAll();

}
