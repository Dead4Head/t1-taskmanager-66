package ru.t1.amsmirnov.taskmanager.dto.response.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.dto.model.ProjectDTO;

public final class ProjectRemoveByIdResponse extends AbstractProjectResponse {

    public ProjectRemoveByIdResponse() {
    }

    public ProjectRemoveByIdResponse(@Nullable final ProjectDTO project) {
        super(project);
    }

    public ProjectRemoveByIdResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

}