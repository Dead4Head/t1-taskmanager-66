package ru.t1.amsmirnov.taskmanager.dto.response.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.dto.model.UserDTO;

public final class UserProfileResponse extends AbstractUserResponse {

    public UserProfileResponse() {
    }

    public UserProfileResponse(@Nullable final UserDTO user) {
        super(user);
    }

    public UserProfileResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

}