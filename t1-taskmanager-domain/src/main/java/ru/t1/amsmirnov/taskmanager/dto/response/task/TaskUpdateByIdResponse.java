package ru.t1.amsmirnov.taskmanager.dto.response.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.dto.model.TaskDTO;

public final class TaskUpdateByIdResponse extends AbstractTaskResponse {

    public TaskUpdateByIdResponse() {
    }

    public TaskUpdateByIdResponse(@Nullable final TaskDTO task) {
        super(task);
    }

    public TaskUpdateByIdResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

}