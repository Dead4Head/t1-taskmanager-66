package ru.t1.amsmirnov.taskmanager.api.model;

import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.enumerated.Role;

public interface ICommand {

    @Nullable
    String getArgument();

    @Nullable
    String getDescription();

    @Nullable
    String getName();

    @Nullable
    Role[] getRoles();


}
