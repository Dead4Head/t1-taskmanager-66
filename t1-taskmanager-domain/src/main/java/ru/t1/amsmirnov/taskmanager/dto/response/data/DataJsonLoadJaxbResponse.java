package ru.t1.amsmirnov.taskmanager.dto.response.data;

import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.dto.response.AbstractResultResponse;

public final class DataJsonLoadJaxbResponse extends AbstractResultResponse {

    public DataJsonLoadJaxbResponse() {
    }

    public DataJsonLoadJaxbResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

}
