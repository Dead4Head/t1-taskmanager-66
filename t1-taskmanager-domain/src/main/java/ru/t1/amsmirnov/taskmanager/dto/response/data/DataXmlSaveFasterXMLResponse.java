package ru.t1.amsmirnov.taskmanager.dto.response.data;

import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.dto.response.AbstractResultResponse;

public final class DataXmlSaveFasterXMLResponse extends AbstractResultResponse {

    public DataXmlSaveFasterXMLResponse() {
    }

    public DataXmlSaveFasterXMLResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

}
