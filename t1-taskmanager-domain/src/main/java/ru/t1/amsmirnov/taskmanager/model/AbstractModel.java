package ru.t1.amsmirnov.taskmanager.model;

import org.jetbrains.annotations.NotNull;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.util.Date;
import java.util.UUID;

@MappedSuperclass
public abstract class AbstractModel {

    @Id
    @NotNull
    protected String id = UUID.randomUUID().toString();

    @NotNull
    @Column(name = "created", nullable = false)
    private Date created = new Date();

    public AbstractModel() {

    }

    @NotNull
    public String getId() {
        return id;
    }

    public void setId(@NotNull final String id) {
        this.id = id;
    }

    @NotNull
    public Date getCreated() {
        return created;
    }

    public void setCreated(@NotNull final Date created) {
        this.created = created;
    }

}
