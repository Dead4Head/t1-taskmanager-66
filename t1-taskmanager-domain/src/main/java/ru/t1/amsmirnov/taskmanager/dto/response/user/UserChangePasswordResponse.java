package ru.t1.amsmirnov.taskmanager.dto.response.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.dto.model.UserDTO;

public final class UserChangePasswordResponse extends AbstractUserResponse {

    public UserChangePasswordResponse() {
    }

    public UserChangePasswordResponse(@Nullable final UserDTO user) {
        super(user);
    }

    public UserChangePasswordResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

}