package ru.t1.amsmirnov.taskmanager.dto.response.data;

import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.dto.response.AbstractResultResponse;

public final class DataBinLoadResponse extends AbstractResultResponse {

    public DataBinLoadResponse() {
    }

    public DataBinLoadResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

}
