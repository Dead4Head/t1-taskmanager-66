package ru.t1.amsmirnov.taskmanager.dto.response.server;

import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.dto.response.AbstractResultResponse;

public class ServerUpdateSchemeResponse extends AbstractResultResponse {

    public ServerUpdateSchemeResponse() {
    }

    public ServerUpdateSchemeResponse(@NotNull Throwable throwable) {
        super(throwable);
    }

}
