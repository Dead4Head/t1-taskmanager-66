package ru.t1.amsmirnov.taskmanager.exception.entity;

public final class TaskNotFoundException extends AbstractEntityNotFoundException {

    public TaskNotFoundException() {
        super("Error! TaskDTO not found...");
    }

}
