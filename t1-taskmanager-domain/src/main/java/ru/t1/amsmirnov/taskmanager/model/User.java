package ru.t1.amsmirnov.taskmanager.model;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.enumerated.Role;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Cacheable
@Table(name = "tm_user")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class User extends AbstractModel {

    @Nullable
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Project> projects = new ArrayList<>();

    @Nullable
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Task> tasks = new ArrayList<>();

    @Nullable
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Session> sessions = new ArrayList<>();

    @Nullable
    @Column(name = "login", nullable = false)
    private String login;

    @Nullable
    @Column(name = "password", nullable = false)
    private String passwordHash;

    @Nullable
    @Column(name = "email", nullable = false)
    private String email;

    @Nullable
    @Column(name = "fst_name")
    private String firstName;

    @Nullable
    @Column(name = "lst_name")
    private String lastName;

    @Nullable
    @Column(name = "mid_name")
    private String middleName;

    @Nullable
    @Column(name = "role", nullable = false)
    @Enumerated(EnumType.STRING)
    private Role role = Role.USUAL;

    @Column(name = "locked", nullable = false)
    private boolean locked = false;

    public User() {

    }

    @Nullable
    public List<Project> getProjects() {
        return projects;
    }

    public void setProjects(@Nullable final List<Project> projects) {
        this.projects = projects;
    }

    @Nullable
    public List<Task> getTasks() {
        return tasks;
    }

    public void setTasks(@Nullable final List<Task> tasks) {
        this.tasks = tasks;
    }

    @Nullable
    public List<Session> getSessions() {
        return sessions;
    }

    public void setSessions(@Nullable final List<Session> sessions) {
        this.sessions = sessions;
    }

    @Nullable
    public String getLogin() {
        return login;
    }

    public void setLogin(@Nullable final String login) {
        this.login = login;
    }

    @Nullable
    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(@Nullable final String passwordHash) {
        this.passwordHash = passwordHash;
    }

    @Nullable
    public String getEmail() {
        return email;
    }

    public void setEmail(@Nullable final String email) {
        this.email = email;
    }

    @Nullable
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(@Nullable final String firstName) {
        this.firstName = firstName;
    }

    @Nullable
    public String getLastName() {
        return lastName;
    }

    public void setLastName(@Nullable final String lastName) {
        this.lastName = lastName;
    }

    @Nullable
    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(@Nullable final String middleName) {
        this.middleName = middleName;
    }

    @Nullable
    public Role getRole() {
        return role;
    }

    public void setRole(@Nullable final Role role) {
        this.role = role;
    }

    public boolean getLocked() {
        return locked;
    }

    public void setLocked(final boolean locked) {
        this.locked = locked;
    }

}
