package ru.t1.amsmirnov.taskmanager.dto.response.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.dto.response.AbstractResultResponse;

public final class TaskClearResponse extends AbstractResultResponse {

    public TaskClearResponse() {
    }

    public TaskClearResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

}