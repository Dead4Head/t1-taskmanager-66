package ru.t1.amsmirnov.taskmanager.service;

import liquibase.Liquibase;
import liquibase.database.Database;
import liquibase.database.DatabaseFactory;
import liquibase.database.jvm.JdbcConnection;
import liquibase.resource.ClassLoaderResourceAccessor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.BeforeClass;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.t1.amsmirnov.taskmanager.configuration.ServerConfiguration;
import ru.t1.amsmirnov.taskmanager.migration.AbstractSchemeTest;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

public abstract class AbstractServiceTest extends AbstractSchemeTest {

    protected static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    protected static final String NONE_STR = "---NONE---";

    @Nullable
    protected static final String NULL_STR = null;

    @NotNull
    final ApplicationContext context = new AnnotationConfigApplicationContext(ServerConfiguration.class);

    @BeforeClass
    public static void initDataBase() throws Exception {
        final Liquibase liquibase = getLiquibase();
        liquibase.dropAll();
        liquibase.update("scheme");
    }

}
