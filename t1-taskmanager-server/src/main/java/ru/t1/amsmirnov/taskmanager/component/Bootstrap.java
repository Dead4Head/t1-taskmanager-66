package ru.t1.amsmirnov.taskmanager.component;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.amsmirnov.taskmanager.api.endpoint.*;
import ru.t1.amsmirnov.taskmanager.api.service.*;
import ru.t1.amsmirnov.taskmanager.api.service.dto.*;
import ru.t1.amsmirnov.taskmanager.dto.model.UserDTO;
import ru.t1.amsmirnov.taskmanager.endpoint.AbstractEndpoint;
import ru.t1.amsmirnov.taskmanager.enumerated.Role;
import ru.t1.amsmirnov.taskmanager.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;

@Component
public final class Bootstrap implements IServiceLocator {

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Autowired
    private IProjectDtoService projectService;

    @NotNull
    @Autowired
    private ITaskDtoService taskService;

    @NotNull
    @Autowired
    private ISessionDtoService sessionService;

    @NotNull
    @Autowired
    private IProjectTaskDtoService projectTaskService;

    @NotNull
    @Autowired
    private IUserDtoService userService;

    @NotNull
    @Autowired
    private IAuthService authService;

    @NotNull
    @Autowired
    private IDomainService domainService;

    @NotNull
    @Autowired
    private ILoggerService loggerService;

    @NotNull
    @Autowired
    private ISystemEndpoint systemEndpoint;

    @NotNull
    @Autowired
    private IDomainEndpoint domainEndpoint;

    @NotNull
    @Autowired
    private IProjectEndpoint projectEndpoint;

    @NotNull
    @Autowired
    private ITaskEndpoint taskEndpoint;

    @NotNull
    @Autowired
    private IUserEndpoint userEndpoint;

    @NotNull
    @Autowired
    private IAuthEndpoint authEndpoint;

    @NotNull
    @Autowired
    private Backup backup;

    @NotNull
    @Autowired
    private AbstractEndpoint[] endpoints;

    private void initEndpoints() {
        Arrays.stream(endpoints).forEach(this::registry);
    }

    private void registry(@NotNull final Object endpoint) {
        @NotNull final String host = propertyService.getServerHost();
        @NotNull final Integer port = propertyService.getServerPort();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String url = "http://" + host + ":" + port + "/" + name + "?wsdl";
        loggerService.info("url: " + url);
        Endpoint.publish(url, endpoint);
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        @NotNull final String fileName = "taskmanager(" + pid + ").pid";
        Files.write(Paths.get(fileName), pid.getBytes());
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }

    private void initDemoData() {
        try {
            @NotNull UserDTO admin = userService.create("admin", "admin", "admin@admin.admin", Role.ADMIN);
            @NotNull UserDTO test = userService.create("test", "test", "test@test.test");
            @NotNull UserDTO user = userService.create("user", "user", "user@user.user");

            projectService.create(test.getId(), "ProjectDTO 1", "Description 1");
            projectService.create(test.getId(), "ProjectDTO 2", "Description 2");
            projectService.create(test.getId(), "ProjectDTO 3", "Description 3");
            projectService.create(admin.getId(), "Admin project", "Description of the admin project");

            taskService.create(test.getId(), "TASK 1", "TASK 1");
            taskService.create(admin.getId(), "TASK 2", "TASK 2");
            taskService.create(user.getId(), "TASK 3", "TASK 3");

        } catch (@NotNull final Exception exception) {
            renderError(exception);
        }
    }

    private void renderError(@NotNull Exception exception) {
        loggerService.error(exception);
        System.out.println("[FAIL]");
    }

    public void start() {
        try {
            initPID();
            initEndpoints();
//          initDemoData();
            loggerService.initJmsLogger();
            loggerService.info("** WELCOME TO TASK MANAGER SERVER**");
            Runtime.getRuntime().addShutdownHook(new Thread(this::stop));
            backup.start();
        } catch (final Exception exception) {
            renderError(exception);
        }
    }

    public void stop() {
        loggerService.info("** TASK MANAGER SERVER IS SHUTTING DOWN **");
        backup.stop();
    }

    @NotNull
    @Override
    public IPropertyService getPropertyService() {
        return propertyService;
    }

    @NotNull
    @Override
    public IProjectDtoService getProjectService() {
        return projectService;
    }

    @NotNull
    @Override
    public ITaskDtoService getTaskService() {
        return taskService;
    }

    @NotNull
    @Override
    public IProjectTaskDtoService getProjectTaskService() {
        return projectTaskService;
    }

    @NotNull
    public ISessionDtoService getSessionService() {
        return sessionService;
    }

    @NotNull
    @Override
    public IUserDtoService getUserService() {
        return userService;
    }

    @NotNull
    @Override
    public IAuthService getAuthService() {
        return authService;
    }

    @NotNull
    @Override
    public IDomainService getDomainService() {
        return domainService;
    }

    @NotNull
    @Override
    public ILoggerService getLoggerService() {
        return loggerService;
    }

}
