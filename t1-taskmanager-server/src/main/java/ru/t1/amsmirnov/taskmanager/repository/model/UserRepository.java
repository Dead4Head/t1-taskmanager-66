package ru.t1.amsmirnov.taskmanager.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.model.User;

import java.util.Optional;

public interface UserRepository extends AbstractRepository<User> {


    @NotNull
    Optional<User> findByLogin(@Nullable final String login);

    @NotNull
    Optional<User> findByEmail(@Nullable final String email);

    boolean existsByLogin(@Nullable final String login);

    boolean existsByEmail(@Nullable final String email);

}
