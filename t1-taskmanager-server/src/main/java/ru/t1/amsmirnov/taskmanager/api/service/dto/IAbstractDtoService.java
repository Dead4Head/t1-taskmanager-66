package ru.t1.amsmirnov.taskmanager.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.dto.model.AbstractModelDTO;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IAbstractDtoService<M extends AbstractModelDTO> {

    @NotNull
    M add(@NotNull M model) throws AbstractException;

    @NotNull
    Collection<M> addAll(@Nullable Collection<M> models) throws AbstractException;

    @NotNull
    Collection<M> set(@Nullable Collection<M> models) throws AbstractException;

    @NotNull
    List<M> findAll() throws AbstractException;

    @NotNull
    List<M> findAll(@Nullable Comparator<M> comparator) throws AbstractException;

    @NotNull
    M findOneById(@Nullable String id) throws AbstractException;

    @NotNull
    M update(@Nullable M model) throws AbstractException;

    void removeAll();

    @NotNull
    M removeOne(@Nullable M model) throws AbstractException;

    @NotNull
    M removeOneById(@Nullable String id) throws AbstractException;

    long getSize() throws AbstractException;

    boolean existById(@NotNull String id) throws AbstractException;

}
