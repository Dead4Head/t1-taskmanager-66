package ru.t1.amsmirnov.taskmanager.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.amsmirnov.taskmanager.api.service.IPropertyService;
import ru.t1.amsmirnov.taskmanager.api.service.dto.IUserDtoService;
import ru.t1.amsmirnov.taskmanager.dto.model.UserDTO;
import ru.t1.amsmirnov.taskmanager.enumerated.Role;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.exception.entity.UserNotFoundException;
import ru.t1.amsmirnov.taskmanager.exception.field.EmailEmptyException;
import ru.t1.amsmirnov.taskmanager.exception.field.LoginEmptyException;
import ru.t1.amsmirnov.taskmanager.exception.field.PasswordEmptyException;
import ru.t1.amsmirnov.taskmanager.exception.user.EmailExistException;
import ru.t1.amsmirnov.taskmanager.exception.user.LoginExistException;
import ru.t1.amsmirnov.taskmanager.repository.dto.UserDtoRepository;
import ru.t1.amsmirnov.taskmanager.util.HashUtil;

import java.util.Optional;

@Service
public final class UserDtoService
        extends AbstractModelDtoService<UserDTO, UserDtoRepository>
        implements IUserDtoService {

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @Autowired
    public UserDtoService(@NotNull final UserDtoRepository repository) {
        super(repository);
    }

    @NotNull
    @Override
    @Transactional
    public UserDTO create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email,
            @Nullable final Role role
    ) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (isLoginExist(login)) throw new LoginExistException();
        if (isEmailExist(email)) throw new EmailExistException();
        @Nullable final String passwordHash = HashUtil.salt(propertyService, password);
        if (passwordHash == null) throw new PasswordEmptyException();
        @NotNull final UserDTO user = new UserDTO();
        user.setLogin(login);
        user.setPasswordHash(passwordHash);
        user.setEmail(email);
        if (role != null) user.setRole(role);
        return add(user);
    }

    @NotNull
    @Override
    @Transactional
    public UserDTO create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) throws AbstractException {
        return create(login, password, email, null);
    }

    @NotNull
    @Override
    public UserDTO findOneByLogin(@Nullable final String login) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final Optional<UserDTO> user = repository.findByLogin(login);
        if (!user.isPresent()) throw new UserNotFoundException();
        return user.get();
    }

    @NotNull
    @Override
    public UserDTO findOneByEmail(@Nullable final String email) throws AbstractException {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        @Nullable final Optional<UserDTO> user = repository.findByEmail(email);
        if (!user.isPresent()) throw new UserNotFoundException();
        return user.get();
    }

    @Override
    public boolean isLoginExist(@Nullable final String login) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        try {
            findOneByLogin(login);
            return true;
        } catch (final AbstractException e) {
            return false;
        }
    }

    @Override
    public boolean isEmailExist(@Nullable final String email) throws AbstractException {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        try {
            findOneByEmail(email);
            return true;
        } catch (final AbstractException e) {
            return false;
        }
    }

    @NotNull
    @Override
    @Transactional
    public UserDTO setPassword(
            @Nullable final String id,
            @Nullable final String password
    ) throws AbstractException {
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull final UserDTO user = findOneById(id);
        @Nullable final String passwordHash = HashUtil.salt(propertyService, password);
        if (passwordHash == null) throw new PasswordEmptyException();
        user.setPasswordHash(passwordHash);
        return update(user);
    }

    @NotNull
    @Override
    @Transactional
    public UserDTO updateUserById(
            @Nullable final String id,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) throws AbstractException {
        @NotNull UserDTO user = findOneById(id);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        return update(user);
    }

    @Override
    @Transactional
    public void lockUserByLogin(final String login) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull final UserDTO user = findOneByLogin(login);
        user.setLocked(true);
        update(user);
    }

    @Override
    @Transactional
    public void unlockUserByLogin(final String login) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull final UserDTO user = findOneByLogin(login);
        user.setLocked(false);
        update(user);
    }

    @NotNull
    @Override
    @Transactional
    public UserDTO removeByLogin(@Nullable final String login) throws AbstractException {
        @NotNull final UserDTO user = findOneByLogin(login);
        return removeOne(user);
    }

    @NotNull
    @Override
    @Transactional
    public UserDTO removeByEmail(@Nullable final String email) throws AbstractException {
        @NotNull final UserDTO user = findOneByEmail(email);
        return removeOne(user);
    }

}
