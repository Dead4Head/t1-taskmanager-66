package ru.t1.amsmirnov.taskmanager.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.amsmirnov.taskmanager.event.ConsoleEvent;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.util.FormatUtil;

@Component
public final class SystemInfoListener extends AbstractSystemListener {

    @NotNull
    public static final String NAME = "info";

    @NotNull
    public static final String DESCRIPTION = "Show system information.";

    @NotNull
    public static final String ARGUMENT = "-i";

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    @EventListener(condition = "@systemInfoListener .isSystemCommand(#consoleEvent.name)")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws AbstractException {
        final int availableProcessors = Runtime.getRuntime().availableProcessors();
        final long freeMemory = Runtime.getRuntime().freeMemory();
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final long totalMemory = Runtime.getRuntime().totalMemory();
        final long usageMemory = totalMemory - freeMemory;

        final boolean maxMemoryCheck = maxMemory == Long.MAX_VALUE;

        @NotNull final String freeMemoryFormat = FormatUtil.formatBytes(freeMemory);
        @NotNull final String maxMemoryFormat = FormatUtil.formatBytes(maxMemory);
        @NotNull final String maxMemoryValue = maxMemoryCheck ? "no limit" : maxMemoryFormat;
        @NotNull final String totalMemoryFormat = FormatUtil.formatBytes(totalMemory);
        @NotNull final String usageMemoryFormat = FormatUtil.formatBytes(usageMemory);

        System.out.println("[INFO]");
        System.out.printf("Available processors (cores): %s \n", availableProcessors);
        System.out.printf("Free memory: %s \n", freeMemoryFormat);
        System.out.printf("Maximum memory: %s \n", maxMemoryValue);
        System.out.printf("Total memory: %s \n", totalMemoryFormat);
        System.out.printf("Usage memory: %s \n", usageMemoryFormat);
    }

}
