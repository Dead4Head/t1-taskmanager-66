package ru.t1.amsmirnov.taskmanager.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.amsmirnov.taskmanager.dto.request.server.ServerDropSchemeRequest;
import ru.t1.amsmirnov.taskmanager.dto.response.server.ServerDropSchemeResponse;
import ru.t1.amsmirnov.taskmanager.event.ConsoleEvent;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.exception.CommandException;

@Component
public final class ServerDropSchemeListener extends AbstractSystemListener {

    @NotNull
    public static final String NAME = "drop-scheme";

    @NotNull
    public static final String DESCRIPTION = "Drop server database.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    @EventListener(condition = "@serverDropSchemeListener .isSystemCommand(#consoleEvent.name)")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws AbstractException {
        System.out.println("[DROP SERVER DATABASE]");
        @NotNull final ServerDropSchemeRequest request = new ServerDropSchemeRequest(getToken());
        @NotNull final ServerDropSchemeResponse response = serviceLocator.getSystemEndpoint().dropScheme(request);
        if (!response.isSuccess())
            throw new CommandException(response.getMessage());
    }

}
