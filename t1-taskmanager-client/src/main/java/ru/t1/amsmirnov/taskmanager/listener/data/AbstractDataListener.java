package ru.t1.amsmirnov.taskmanager.listener.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.amsmirnov.taskmanager.api.endpoint.IDomainEndpoint;
import ru.t1.amsmirnov.taskmanager.listener.AbstractListener;
import ru.t1.amsmirnov.taskmanager.enumerated.Role;

@Component
public abstract class AbstractDataListener extends AbstractListener {

    @NotNull
    @Autowired
    public IDomainEndpoint domainEndpoint;

    public AbstractDataListener() {
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
