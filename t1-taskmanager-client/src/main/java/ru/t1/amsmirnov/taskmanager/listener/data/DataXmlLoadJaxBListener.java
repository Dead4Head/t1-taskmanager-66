package ru.t1.amsmirnov.taskmanager.listener.data;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.amsmirnov.taskmanager.dto.request.data.DataXmlLoadJaxBRequest;
import ru.t1.amsmirnov.taskmanager.dto.response.data.DataXmlLoadJaxBResponse;
import ru.t1.amsmirnov.taskmanager.event.ConsoleEvent;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.exception.CommandException;

@Component
public final class DataXmlLoadJaxBListener extends AbstractDataListener {

    @NotNull
    public static final String NAME = "data-load-xml-jaxb";

    @NotNull
    public static final String DESCRIPTION = "Load data from XML file.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@dataXmlLoadJaxBListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws AbstractException {
        System.out.println("[DATA LOAD XML]");
        @NotNull final DataXmlLoadJaxBRequest request = new DataXmlLoadJaxBRequest(getToken());
        @NotNull final DataXmlLoadJaxBResponse response = domainEndpoint.loadDataXmlJaxB(request);
        if (!response.isSuccess())
            throw new CommandException(response.getMessage());
    }

}
